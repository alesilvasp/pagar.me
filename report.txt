Creating test database for alias 'default' ('file:memorydb_default?mode=memory&cache=shared')...
Found 75 test(s).
Operations to perform:
  Synchronize unmigrated apps: messages, rest_framework, staticfiles
  Apply all migrations: admin, auth, authtoken, contenttypes, fee, payment, product, sessions, user
Synchronizing apps without migrations:
  Creating tables...
    Running deferred SQL...
Running migrations:
  Applying contenttypes.0001_initial... OK
  Applying contenttypes.0002_remove_content_type_name... OK
  Applying auth.0001_initial... OK
  Applying auth.0002_alter_permission_name_max_length... OK
  Applying auth.0003_alter_user_email_max_length... OK
  Applying auth.0004_alter_user_username_opts... OK
  Applying auth.0005_alter_user_last_login_null... OK
  Applying auth.0006_require_contenttypes_0002... OK
  Applying auth.0007_alter_validators_add_error_messages... OK
  Applying auth.0008_alter_user_username_max_length... OK
  Applying auth.0009_alter_user_last_name_max_length... OK
  Applying auth.0010_alter_group_name_max_length... OK
  Applying auth.0011_update_proxy_permissions... OK
  Applying auth.0012_alter_user_first_name_max_length... OK
  Applying user.0001_initial... OK
  Applying admin.0001_initial... OK
  Applying admin.0002_logentry_remove_auto_add... OK
  Applying admin.0003_logentry_add_action_flag_choices... OK
  Applying authtoken.0001_initial... OK
  Applying authtoken.0002_auto_20160226_1747... OK
  Applying authtoken.0003_tokenproxy... OK
  Applying fee.0001_initial... OK
  Applying fee.0002_alter_fee_credit_fee_alter_fee_debit_fee... OK
  Applying fee.0003_rename_uuid_fee_id... OK
  Applying fee.0004_alter_fee_credit_fee_alter_fee_debit_fee... OK
  Applying fee.0005_auto_20220323_1411... OK
  Applying payment.0001_initial... OK
  Applying payment.0002_remove_paymentinfo_id_paymentinfo_uuid... OK
  Applying payment.0003_alter_paymentinfo_card_expiring_date... OK
  Applying payment.0004_alter_paymentinfo_card_expiring_date... OK
  Applying payment.0005_alter_paymentinfo_is_active... OK
  Applying payment.0006_alter_paymentinfo_customer... OK
  Applying payment.0007_alter_paymentinfo_card_number... OK
  Applying payment.0008_alter_paymentinfo_card_number... OK
  Applying payment.0009_rename_uuid_paymentinfo_id... OK
  Applying product.0001_initial... OK
  Applying product.0002_rename_uuid_product_id... OK
  Applying sessions.0001_initial... OK
System check identified no issues (0 silenced).
test_fee_fields (fee.tests.tests_model.FeeModelTest) ... ok
test_admin_create_new_fee (fee.tests.tests_view.FeeViewTest) ... ok
test_list_fee (fee.tests.tests_view.FeeViewTest) ... ok
test_not_admin_cannot_create_new_fee (fee.tests.tests_view.FeeViewTest) ... ok
test_retrieve_fee_by_id (fee.tests.tests_view.FeeViewTest) ... ok
test_wrong_or_missing_fields (fee.tests.tests_view.FeeViewTest) ... ok
test_payment_info_fields (payment.tests.tests_model.PaymentInfoModelTest) ... ok
test_admin_or_seller_cannot_create_payment_info (payment.tests.tests_view.PaymentInfoTestView) ... ok
test_buyer_create_payment_info (payment.tests.tests_view.PaymentInfoTestView) ... ok
test_card_expired (payment.tests.tests_view.PaymentInfoTestView) ... ok
test_same_card_number_creation (payment.tests.tests_view.PaymentInfoTestView) ... ok
test_product_fields (product.tests.tests_model.ProductModelTest) ... ok
test_admin_cannot_create_product (product.tests.tests_view.ProductCreateTestView) ... ok
test_create_new_product (product.tests.tests_view.ProductCreateTestView) ... ok
test_list_all_products (product.tests.tests_view.ProductRetrieveAndUpdateTest) ... ok
test_list_products_by_seller (product.tests.tests_view.ProductRetrieveAndUpdateTest) ... ok
test_retrieve_product_by_id (product.tests.tests_view.ProductRetrieveAndUpdateTest) ... ok
test_update_product (product.tests.tests_view.ProductRetrieveAndUpdateTest) ... ok
test_admin_list_users_success_200 (test_api.TestAccounst) ... ok
test_anonymous_list_users_fail_401 (test_api.TestAccounst) ... ok
test_buyer_list_users_fail_403 (test_api.TestAccounst) ... ok
test_create_account_admin_success_201 (test_api.TestAccounst) ... ok
test_create_account_buyer_success_201 (test_api.TestAccounst) ... ok
test_create_account_duplicated_email_fail_400 (test_api.TestAccounst) ... ok
test_create_account_seller_success_201 (test_api.TestAccounst) ... ok
test_create_account_wrong_fields_fail_400 (test_api.TestAccounst) ... ok
test_login_fail_401 (test_api.TestAccounst) ... ok
test_login_failure_wrong_fields_400 (test_api.TestAccounst) ... ok
test_login_success_200 (test_api.TestAccounst) ... ok
test_seller_list_users_fail_403 (test_api.TestAccounst) ... ok
test_admin_create_fee_success_201 (test_api.TestFee) ... ok
test_admin_create_fee_with_wrong_fields_fail_400 (test_api.TestFee) ... ok
test_admin_list_fee_success_200 (test_api.TestFee) ... ok
test_admin_retrieve_fee_success_200 (test_api.TestFee) ... ok
test_anonymous_create_fee_fail_401 (test_api.TestFee) ... ok
test_anonymous_list_fee_fail_401 (test_api.TestFee) ... ok
test_anonymous_retrieve_fee_fail_401 (test_api.TestFee) ... ok
test_buyer_create_fee_fail_403 (test_api.TestFee) ... ok
test_buyer_list_fee_fail_403 (test_api.TestFee) ... ok
test_buyer_retrieve_fee_fail_403 (test_api.TestFee) ... ok
test_seller_create_fee_fail_403 (test_api.TestFee) ... ok
test_seller_list_fee_fail_403 (test_api.TestFee) ... ok
test_seller_retrieve_fee_fail_403 (test_api.TestFee) ... ok
test_admin_create_payment_info_fail_403 (test_api.TestPaymentInfo) ... ok
test_admin_list_payment_info_fail_403 (test_api.TestPaymentInfo) ... ok
test_anonymous_create_payment_info_fail_401 (test_api.TestPaymentInfo) ... ok
test_anonymous_list_payment_info_fail_401 (test_api.TestPaymentInfo) ... ok
test_buyer_create_payment_info_already_exists_fail_422 (test_api.TestPaymentInfo) ... ok
test_buyer_create_payment_info_success_200 (test_api.TestPaymentInfo) ... ok
test_buyer_create_payment_info_with_wrong_fields_fail_400 (test_api.TestPaymentInfo) ... ok
test_buyer_list_payment_info_success_200 (test_api.TestPaymentInfo) ... ok
test_seller_create_payment_info_fail_403 (test_api.TestPaymentInfo) ... ok
test_seller_list_payment_info_fail_403 (test_api.TestPaymentInfo) ... ok
test_admin_create_product_fail_403 (test_api.TestProducts) ... ok
test_admin_update_product_fail_403 (test_api.TestProducts) ... ok
test_anonymous_create_product_fail_401 (test_api.TestProducts) ... ok
test_anonymous_update_product_fail_401 (test_api.TestProducts) ... ok
test_buyer_create_product_fail_403 (test_api.TestProducts) ... ok
test_buyer_update_product_fail_403 (test_api.TestProducts) ... ok
test_list_products_200 (test_api.TestProducts) ... ok
test_list_products_by_seller_200 (test_api.TestProducts) ... ok
test_list_products_by_seller_with_wrong_seller_id_404 (test_api.TestProducts) ... ok
test_retrieve_products_fail_wrong_id_404 (test_api.TestProducts) ... ok
test_retrieve_products_success_200 (test_api.TestProducts) ... ok
test_seller_create_product_success_201 (test_api.TestProducts) ... ok
test_seller_create_product_with_wrong_fields_fail_400 (test_api.TestProducts) ... ok
test_seller_update_product_success_200 (test_api.TestProducts) ... ok
test_seller_update_product_with_wrong_product_id_404 (test_api.TestProducts) ... ok
test_user_fields (user.tests.tests_model.UserModelTest) ... ok
test_login_fail (user.tests.tests_view.LoginTestView) ... ok
test_login_success (user.tests.tests_view.LoginTestView) ... ok
test_create_new_user_fail (user.tests.tests_view.UserViewTest) ... ok
test_create_new_user_success (user.tests.tests_view.UserViewTest) ... ok
test_not_admin_cannot_read_users (user.tests.tests_view.UserViewTest) ... ok
test_only_admin_can_read_users (user.tests.tests_view.UserViewTest) ... ok

----------------------------------------------------------------------
Ran 75 tests in 11.613s

OK
Destroying test database for alias 'default' ('file:memorydb_default?mode=memory&cache=shared')...
