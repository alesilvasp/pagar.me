from django.forms import ValidationError
from rest_framework import serializers

from .models import Product
from user.serializers import UserSerializer


class ProductSerializer(serializers.ModelSerializer):
  seller = UserSerializer(read_only=True)

  class Meta:
    model = Product
    fields = '__all__'

    extra_kwargs = {
        'seller': {'read_only': True}
    }

  def validate(self, data):
    if hasattr(self, 'initial_data'):
      unknown_keys = set(self.initial_data.keys()) - set(self.fields.keys())
      if unknown_keys:
        raise ValidationError("Got unknown fields: {}".format(unknown_keys))
    return data

  def create(self, validated_data):
    return Product.objects.create(**validated_data, seller=self.context['request'].user)

  def update(self, instance, validated_data):
    validated_data['seller'] = self.context['request'].user
    return super().update(instance, validated_data)


class ProductSellerListSerializer(serializers.ModelSerializer):
  seller = serializers.PrimaryKeyRelatedField(read_only=True)

  class Meta:
    model = Product
    fields = '__all__'
