from django.shortcuts import get_object_or_404
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateAPIView, ListAPIView
from rest_framework.authentication import TokenAuthentication
from pagar_me.permissions import IsSellerOrReadOnly
from user.models import User

from .models import Product
from .serializers import ProductSerializer, ProductSellerListSerializer


class ProductListCreate(ListCreateAPIView):

  queryset = Product.objects.all()
  serializer_class = ProductSerializer

  authentication_classes = [TokenAuthentication]
  permission_classes = [IsSellerOrReadOnly]

  def get_serializer_class(self):
    if self.request.method == 'GET':
      return ProductSellerListSerializer
    return super().get_serializer_class()


class ProductRetrieveAndUpdate(RetrieveUpdateAPIView):
  queryset = Product.objects.all()
  serializer_class = ProductSerializer
  lookup_url_kwarg = "product_id"

  authentication_classes = [TokenAuthentication]
  permission_classes = [IsSellerOrReadOnly]

  def get_serializer_class(self):
    if self.request.method == 'GET':
      return ProductSellerListSerializer
    return super().get_serializer_class()


class ProductListBySellerView(ListAPIView):

  queryset = Product.objects.all()
  serializer_class = ProductSellerListSerializer

  authentication_classes = [TokenAuthentication]
  permission_classes = [IsSellerOrReadOnly]

  lookup_url_kwarg = "seller_id"

  def get_queryset(self):
    self.seller = get_object_or_404(User, id=self.kwargs['seller_id'])
    return Product.objects.filter(seller=self.seller)
