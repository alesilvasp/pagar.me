from django.test import TestCase

from user.models import User
from product.models import Product

class ProductModelTest(TestCase):
  @classmethod
  def setUpTestData(cls) -> None:
    
    cls.email = 'email@email.com'
    cls.password = '1234'
    cls.first_name = 'alex'
    cls.last_name = 'silva'
    cls.is_seller = True
    cls.is_admin = False

    cls.user = User.objects.create_user(
        email=cls.email,
        password=cls.password,
        first_name=cls.first_name,
        last_name=cls.last_name,
        is_seller=cls.is_seller,
        is_admin=cls.is_admin
    )

    cls.description = "description"
    cls.price = 50.50
    cls.quantity = 10
    
    cls.product = Product.objects.create(
      description = cls.description,
      price = cls.price,
      quantity = cls.quantity,
      seller = cls.user
    )
    
  def test_product_fields(self):
    self.assertIsInstance(self.product.description, str)
    self.assertIsInstance(self.product.price, float)
    self.assertIsInstance(self.product.quantity, int)
 
    self.assertEqual(self.product.is_active, True)
    
    self.assertEqual(self.product.description, self.description)
    self.assertEqual(self.product.quantity, self.quantity)
    self.assertEqual(self.product.price, self.price)
    self.assertEqual(self.product.seller.email, self.email)