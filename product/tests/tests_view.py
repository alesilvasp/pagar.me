from rest_framework.test import APITestCase
from rest_framework.authtoken.models import Token

from product.models import Product
from user.models import User


class ProductCreateTestView(APITestCase):
  def setUp(self) -> None:
    self.user_seller = User.objects.create_user(
        email='anotadmin@gmail.com',
        password='1234',
        first_name='Alex',
        last_name='Silva',
        is_admin=False,
        is_seller=True,
    )

    self.user_admin = User.objects.create_user(
        email='admin@gmail.com',
        password='1234',
        first_name='Admin',
        last_name='Silva',
        is_admin=True,
        is_seller=False,
    )

  def test_create_new_product(self):
    self.new_product = {
        'description': 'desc',
        'price': 50.50,
        'quantity': 100,
        'seller': self.user_seller
    }

    token = Token.objects.create(user=self.user_seller)

    self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

    response = self.client.post('/api/products/', self.new_product)

    self.assertEqual(response.status_code, 201)
    self.assertIn('is_active', response.json())
    self.assertIsInstance(response.json().get('is_active'), bool)

  def test_admin_cannot_create_product(self):
    self.new_product = {
        'description': 'desc',
        'price': 50.50,
        'quantity': 100,
        'seller': self.user_admin
    }

    token = Token.objects.create(user=self.user_admin)

    self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

    response = self.client.post('/api/products/', self.new_product)

    self.assertEqual(response.status_code, 403)


class ProductRetrieveAndUpdateTest(APITestCase):
  def setUp(self) -> None:
    self.user_seller = User.objects.create_user(
        email='anotadmin@gmail.com',
        password='1234',
        first_name='Alex',
        last_name='Silva',
        is_admin=False,
        is_seller=True,
    )
    
    self.user_seller_2 = User.objects.create_user(
        email='anotherseller@gmail.com',
        password='1234',
        first_name='john',
        last_name='doe',
        is_admin=False,
        is_seller=True,
    )

    self.new_product = Product.objects.create(
        description='desc',
        price=10.50,
        quantity=10,
        seller=self.user_seller
    )

    self.new_product2 = Product.objects.create(
        description='desc2',
        price=102.50,
        quantity=102,
        seller=self.user_seller
    )
    
    self.new_product3 = Product.objects.create(
        description='desc3',
        price=20.50,
        quantity=500,
        seller=self.user_seller_2
    )

  def test_list_all_products(self):

    response = self.client.get('/api/products/')

    self.assertEqual(response.status_code, 200)
    self.assertEqual(len(response.json()), 3)
    self.assertIsInstance(response.json(), list)

  def test_retrieve_product_by_id(self):

    response = self.client.get(f'/api/products/{self.new_product2.id}/')

    self.assertEqual(response.status_code, 200)
    self.assertEqual(response.json()['quantity'], self.new_product2.quantity)

  def test_update_product(self):

    self.to_update = {
      'description': 'new description'
    }

    token = Token.objects.create(user=self.user_seller)

    self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')
    
    response = self.client.patch(f'/api/products/{self.new_product.id}/', self.to_update, format='json')
    output = response.json()
    
    self.assertEqual(response.status_code, 200)
    self.assertEqual(output['description'], str(self.to_update['description']))
    
    # Verificando se foi atualizado no banco
    response_updated = self.client.get(f'/api/products/{self.new_product.id}/')
    output = response_updated.json()
    
    self.assertEqual(output['description'], self.to_update['description'])
    
    
  def test_list_products_by_seller(self):
    
    token = Token.objects.create(user=self.user_seller)

    self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')
    
    response = self.client.get(f'/api/products/seller/{self.user_seller.id}/', format="json")
    output = response.json()
    
    self.assertIsInstance(output, list)
    self.assertEqual(len(output), 2)