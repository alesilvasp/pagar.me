from django.urls import path
from .views import ProductListCreate, ProductRetrieveAndUpdate, ProductListBySellerView

urlpatterns = [
    path('products/', ProductListCreate.as_view()),
    path('products/<product_id>/', ProductRetrieveAndUpdate.as_view()),
    path('products/seller/<seller_id>/', ProductListBySellerView.as_view())
]