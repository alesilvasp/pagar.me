from django.db import models
import uuid


class Product(models.Model):

  id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
  description = models.CharField(max_length=255)
  price = models.FloatField()
  quantity = models.IntegerField()
  is_active = models.BooleanField(default=True)
  seller = models.ForeignKey(
      'user.User', related_name='products', on_delete=models.CASCADE)

