from rest_framework.generics import ListCreateAPIView, RetrieveAPIView
from rest_framework.authentication import TokenAuthentication
from pagar_me.permissions import IsAdmin 

from .models import Fee
from .serializers import FeeSerializer

class FeeView(ListCreateAPIView):
  
  queryset = Fee.objects.all()
  serializer_class = FeeSerializer
  
  authentication_classes = [TokenAuthentication]
  permission_classes = [IsAdmin]
  
class FeeRetrieveView(RetrieveAPIView):
  queryset = Fee.objects.all()
  serializer_class = FeeSerializer
  lookup_url_kwarg = "fee_id"
  
  authentication_classes = [TokenAuthentication]
  permission_classes = [IsAdmin]