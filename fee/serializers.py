from rest_framework import serializers

from .models import Fee


class FeeSerializer(serializers.ModelSerializer):
  class Meta:
    model = Fee
    fields = '__all__'

    extra_kwargs = {
        'id': {'read_only': True},
        'created_at': {'read_only': True}
    }
