from rest_framework.test import APITestCase
from rest_framework.authtoken.models import Token

from fee.models import Fee
from user.models import User


class FeeViewTest(APITestCase):
  def setUp(self) -> None:
    self.user_admin = User.objects.create_user(
        email='admin@gmail.com',
        password='1234',
        first_name='Alex',
        last_name='Silva',
        is_admin=True,
        is_seller=False,
    )
    
    self.user_not_admin = User.objects.create_user(
        email='notadmin@gmail.com',
        password='1234',
        first_name='Alex',
        last_name='Silva',
        is_admin=False,
        is_seller=True,
    )

  def test_admin_create_new_fee(self):
    fee = {
        "credit_fee": 0.06,
        "debit_fee": 0.04,
    }

    token = Token.objects.create(user=self.user_admin)

    self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')
    
    response = self.client.post('/api/fee/', fee)
    
    self.assertEqual(response.status_code, 201)
    self.assertIn('created_at', response.json())

  def test_not_admin_cannot_create_new_fee(self):
    fee = {
        "credit_fee": 0.06,
        "debit_fee": 0.04,
    }

    token = Token.objects.create(user=self.user_not_admin)

    self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')
    
    response = self.client.post('/api/fee/', fee)
    
    self.assertEqual(response.status_code, 403)
    
  def test_wrong_or_missing_fields(self):
    wrong_fee = {
        "credit_fe": 0.06,
        "debit_fee": 0.04,
    }
    
    missing_fee = {
      "credit_fee": 0.06
    }

    token = Token.objects.create(user=self.user_admin)

    self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')
    
    response1 = self.client.post('/api/fee/', wrong_fee)
    response2 = self.client.post('/api/fee/', missing_fee)
    
    self.assertEqual(response1.status_code, 400)
    self.assertEqual(response2.status_code, 400)
    
  def test_list_fee(self):
    self.new_fee = Fee.objects.create(
      credit_fee = 0.05,
      debit_fee=0.03
    )
    
    token = Token.objects.create(user=self.user_admin)

    self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')
    
    response = self.client.get(f'/api/fee/')
    
    self.assertEqual(response.status_code, 200)
    self.assertEqual(len(response.json()), 2)

  def test_retrieve_fee_by_id(self):
    self.new_fee = Fee.objects.create(
      credit_fee = '0.05',
      debit_fee='0.03'
    )
    
    token = Token.objects.create(user=self.user_admin)

    self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')
    
    response = self.client.get(f'/api/fee/{self.new_fee.id}/')
    
    self.assertEqual(response.status_code, 200)
    self.assertEqual(response.json()['credit_fee'], self.new_fee.credit_fee)
    