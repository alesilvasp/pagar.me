from django.test import TestCase

from fee.models import Fee


class FeeModelTest(TestCase):
  @classmethod
  def setUpTestData(cls) -> None:
    cls.credit_fee = 0.05
    cls.debit_fee = 0.03

    cls.fee = Fee.objects.create(
        credit_fee=cls.credit_fee,
        debit_fee=cls.debit_fee,
    )

  def test_fee_fields(self):
    self.assertIsInstance(self.fee.credit_fee, float)
    self.assertIsInstance(self.fee.debit_fee, float)

    self.assertEqual(self.fee.credit_fee, self.credit_fee)
    self.assertEqual(self.fee.debit_fee, self.debit_fee)
