from django.db import models
import uuid

# Create your models here.


class Fee(models.Model):
  id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
  credit_fee = models.CharField(max_length=255)
  debit_fee = models.CharField(max_length=255)
  created_at = models.DateTimeField(auto_now_add=True)

