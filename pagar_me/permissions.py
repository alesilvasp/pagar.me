from rest_framework.permissions import BasePermission, SAFE_METHODS


class IsAdmin(BasePermission):
  def has_permission(self, request, view):

    if request.user.is_authenticated:
      return request.user.is_admin
    
class IsCustomer(BasePermission):
  def has_permission(self, request, view):

    if request.user.is_authenticated:
      return bool(not request.user.is_admin and not request.user.is_seller)


class IsSellerOrReadOnly(BasePermission):
  def has_permission(self, request, view):
    if request.method in SAFE_METHODS:
      return True

    if request.user.is_authenticated:
      
      return request.user.is_seller


class IsCustomerOrReadOnly(BasePermission):
  def has_permission(self, request, view):
    if request.method in SAFE_METHODS:
      return True
    
    if request.user.is_authenticated:

      return bool(not request.user.is_admin and not request.user.is_seller)


class IsUserCreationOrList(BasePermission):
  def has_permission(self, request, view):

    if request.method == 'POST':
      return True

    return bool(request.user.is_authenticated and
                request.user.is_admin == True)
