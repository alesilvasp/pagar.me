from email.policy import default
from rest_framework.exceptions import APIException


class DuplicateEmailException(APIException):
  status_code = 400
  default_detail = {
      "email": [
          "user with this email already exists."
      ]
  }


class InvalidCredentials(APIException):
  status_code = 401
  default_detail = {
      "error": [
          "Invalid email or password"
      ]
  }


class DuplicateCardUser(APIException):
  status_code = 422
  default_detail = {"error": ["This card is already registered for this user"]}
