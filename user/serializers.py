from rest_framework import serializers
from .models import User
from pagar_me.exceptions import DuplicateEmailException


class UserSerializer(serializers.ModelSerializer):
  class Meta:
    model = User
    fields = ['id', 'email', 'password', 'first_name',
              'last_name', 'is_admin', 'is_seller']
    extra_kwargs = {
        'id': {'read_only': True},
        'password': {'write_only': True},
        'email': {'validators': []}
    }

  def validate(self, attrs):
    email = attrs['email']
    user = User.objects.filter(email=email).first()

    if user:
      raise DuplicateEmailException()
    return super().validate(attrs)

  def create(self, validated_data):
    user = User.objects.create_user(**validated_data)
    return user


class LoginSerializer(serializers.Serializer):
  email = serializers.EmailField()
  password = serializers.CharField()
