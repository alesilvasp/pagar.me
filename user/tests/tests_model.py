from django.test import TestCase

from user.models import User


class UserModelTest(TestCase):
  @classmethod
  def setUpTestData(cls) -> None:
    cls.email = 'email@email.com'
    cls.password = '1234'
    cls.first_name = 'alex'
    cls.last_name = 'silva'
    cls.is_seller = False
    cls.is_admin = True

    cls.user = User.objects.create_user(
        email=cls.email,
        password=cls.password,
        first_name=cls.first_name,
        last_name=cls.last_name,
        is_seller = cls.is_seller,
        is_admin = cls.is_admin
    )

  def test_user_fields(self):
    self.assertIsInstance(self.user.email, str)
    self.assertEqual(self.user.email, self.email)
    self.assertIsInstance(self.is_seller, bool)
    self.assertEqual(self.user.is_admin, True)

    self.assertIsInstance(self.user.password, str)
