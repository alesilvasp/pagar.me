from rest_framework.generics import ListCreateAPIView
from rest_framework.authentication import TokenAuthentication
from pagar_me.permissions import IsCustomer

from .models import PaymentInfo
from .serializers import PaymentInfoSerializer, CustomerPaymentInfoListSerializer

class PaymentInfoCreateListView(ListCreateAPIView):
  
  queryset = PaymentInfo.objects.all()
  serializer_class = PaymentInfoSerializer
  
  authentication_classes = [TokenAuthentication]
  permission_classes = [IsCustomer]
  
  def get_serializer_class(self):
    if self.request.method == 'GET':
      return CustomerPaymentInfoListSerializer
    return super().get_serializer_class()
