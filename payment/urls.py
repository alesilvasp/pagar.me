from django.urls import path
from .views import PaymentInfoCreateListView

urlpatterns = [
    path('payment_info/', PaymentInfoCreateListView.as_view()),
    
]