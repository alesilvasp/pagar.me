from datetime import date
from django.test import TestCase

from user.models import User
from payment.models import PaymentInfo


class PaymentInfoModelTest(TestCase):
  @classmethod
  def setUpTestData(cls) -> None:
    
    cls.email = 'email@email.com'
    cls.password = '1234'
    cls.first_name = 'alex'
    cls.last_name = 'silva'
    cls.is_seller = False
    cls.is_admin = False

    cls.user = User.objects.create_user(
        email=cls.email,
        password=cls.password,
        first_name=cls.first_name,
        last_name=cls.last_name,
        is_seller=cls.is_seller,
        is_admin=cls.is_admin
    )

    cls.payment_method = "debit"
    cls.card_number = "1234567812345678"
    cls.cardholders_name = "MARIANA F SOUZA"
    cls.card_expiring_date = date.fromisoformat("2020-04-15")
    cls.cvv = '456'
    
    cls.payment_info = PaymentInfo.objects.create(
      payment_method = cls.payment_method,
      card_number = cls.card_number,
      cardholders_name = cls.cardholders_name,
      card_expiring_date = cls.card_expiring_date,
      cvv = cls.cvv,
      customer = cls.user
    )
    
  def test_payment_info_fields(self):
    self.assertIsInstance(self.payment_info.payment_method, str)
    self.assertIsInstance(self.payment_info.card_number, str)
    self.assertIsInstance(self.payment_info.cardholders_name, str)
    self.assertIsInstance(self.payment_info.card_expiring_date, date)
    self.assertIsInstance(self.payment_info.cvv, str)
    self.assertEqual(self.payment_info.is_active, True)
    
    self.assertEqual(self.payment_info.payment_method, self.payment_method)
    self.assertEqual(self.payment_info.customer.email, self.email)