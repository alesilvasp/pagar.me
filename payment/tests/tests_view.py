from rest_framework.test import APITestCase
from rest_framework.authtoken.models import Token

from payment.models import PaymentInfo
from user.models import User


class PaymentInfoTestView(APITestCase):
  def setUp(self) -> None:
    self.user_customer = User.objects.create_user(
        email='customer@gmail.com',
        password='1234',
        first_name='Alex',
        last_name='Silva',
        is_admin=False,
        is_seller=False,
    )

    self.user_not_customer = User.objects.create_user(
        email='notcustomer@gmail.com',
        password='1234',
        first_name='John',
        last_name='Doe',
        is_admin=False,
        is_seller=True,
    )

  def test_buyer_create_payment_info(self):
    self.payment_info = {
        "payment_method": "debit",
        "card_number": "1234567812345678",
        "cardholders_name": "Alexander P Silva",
        "card_expiring_date": "2022-04-01",
        "cvv": "456",
        "customer": self.user_customer
    }

    token = Token.objects.create(user=self.user_customer)

    self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

    response = self.client.post('/api/payment_info/', self.payment_info)
    output = response.json()

    self.assertEqual(response.status_code, 201)
    self.assertIn('is_active', response.json())

  def test_admin_or_seller_cannot_create_payment_info(self):
    self.payment_info = {
        "payment_method": "debit",
        "card_number": "1234567812345678",
        "cardholders_name": "Alexander P Silva",
        "card_expiring_date": "2022-04-01",
        "cvv": "456",
        "customer": self.user_customer
    }

    token = Token.objects.create(user=self.user_not_customer)

    self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

    response = self.client.post('/api/payment_info/', self.payment_info)

    self.assertEqual(response.status_code, 403)

  def test_card_expired(self):
    self.payment_info = {
        "payment_method": "debit",
        "card_number": "1234567812345678",
        "cardholders_name": "Alexander P Silva",
        "card_expiring_date": "2022-02-01",
        "cvv": "456",
        "customer": self.user_customer
    }

    token = Token.objects.create(user=self.user_customer)

    self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

    response = self.client.post('/api/payment_info/', self.payment_info)

    self.assertEqual(response.status_code, 400)
    
  def test_same_card_number_creation(self):
    self.payment_info = {
        "payment_method": "debit",
        "card_number": "12345678",
        "cardholders_name": "Alexander P Silva",
        "card_expiring_date": "2022-05-01",
        "cvv": "456",
        "customer": self.user_customer
    }

    token = Token.objects.create(user=self.user_customer)

    self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

    response = self.client.post('/api/payment_info/', self.payment_info)
    
    response2 = self.client.post('/api/payment_info/', self.payment_info)
    

    self.assertEqual(response2.status_code, 422)
