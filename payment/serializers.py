from rest_framework import serializers
from datetime import date
from rest_framework.exceptions import ValidationError
from pagar_me.exceptions import DuplicateCardUser
from user.models import User

from .models import PaymentInfo
from user.serializers import UserSerializer


class PaymentInfoSerializer(serializers.ModelSerializer):
  customer = UserSerializer(read_only=True)

  card_number_info = serializers.SerializerMethodField()

  class Meta:
    model = PaymentInfo
    fields = '__all__'

    extra_kwargs = {
        'id': {'read_only': True},
        'is_active': {'read_only': True},
        'customer': {'read_only': True},
        'card_number': {'write_only': True},
        'cvv': {'write_only': True}
    }
    
  def get_card_number_info(self, obj):
    return obj.card_number[-4:]

  def validate(self, attrs):
    now = date.today()
    card_date = attrs['card_expiring_date']
    card_number = attrs['card_number']
    if card_date < now:
      raise ValidationError({"error": ["This card is expired"]})
    
    customer = User.objects.get(email=self.context['request'].user)
    payments = PaymentInfo.objects.filter(customer=customer)
    
    for p in payments:
      if card_number == p.card_number:
        raise DuplicateCardUser()
    return super().validate(attrs)

  def create(self, validated_data):
    return PaymentInfo.objects.create(**validated_data, customer=self.context['request'].user)

class CustomerPaymentInfoListSerializer(serializers.ModelSerializer):
  customer = serializers.PrimaryKeyRelatedField(read_only=True)

  card_number_info = serializers.SerializerMethodField()

  class Meta:
    model = PaymentInfo
    fields = '__all__'
    
    extra_kwargs = {
        'id': {'read_only': True},
        'is_active': {'read_only': True},
        'customer': {'read_only': True},
        'card_number': {'write_only': True},
        'cvv': {'write_only': True}
    }
    
  def get_card_number_info(self, obj):
    return obj.card_number[-4:]