# Pagar.me

Essa api foi desenvolvida com objetivos acadêmicos, proposta pela Kenzie Academy.

## Como instalar?

Faça o clone do repositório:

<https://gitlab.com/alesilvasp/pagar.me>

Entre na pasta instale e ative o ambiente virtual no terminal:

`python -m venv venv --upgrade-deps`

`source venv/bin/activate`

Instale as dependências com o comando:

`pip install -r requirements.txt`

Depois de instaladas as dependências, rode o comando no terminal:

`./manage.py runserver`

O sistema estará rodando em http://127.0.0.1:8000/

## Utilização

Para utilizar este sistema, é necessário utilizar um API Client, como o Insomnia ou Postman

## Rotas

### Accounts

**POST /api/accounts**

Feita para cadastrar um novo usuário

A API possui 3 tipos de usuário:

Admin (caso a chave `is_admin=True`);

Seller (caso a chave `is_seller=True`);

Customer (caso as chaves `is_admin` e `is_seller` sejam `False`)

RESPONSE STATUS -> HTTP 201(CREATED)

Body:

```json
{
 "email": "customer@gmail.com",
 "password": "1234",
 "first_name": "customer",
 "last_name": "one",
 "is_seller": false,
 "is_admin": false
}
```

Response:

```json
{
 "uuid": "5bfaaea7-656a-4435-a87b-03526c6dc821",
 "email": "customer@gmail.com",
 "first_name": "customer",
 "last_name": "one",
 "is_admin": false,
 "is_seller": false
}
```

**POST api/login**

Logar na api informando _email_ e _password_

RESPONSE STATUS -> HTTP 200(OK)

Body:

```json
{
 "email": "customer@gmail.com",
 "password": "1234"
}
```

Response:

```json
{
 "Token": "49c41264389226a8033f416d8e7eaf7f3b484f28"
}
```

**GET api/users**

Retorno de todos os usuários cadastrados

Authorization Header:

**Necessário passar o token para autenticação**

**Somente o Administrador tem acesso a rota**

RESPONSE STATUS -> HTTP 200(OK)

Body:

_No body_

Response:

```json
[
 {
  "uuid": "437e93b6-84b7-4f93-9a59-a2ae05ab0e09",
  "email": "jose@bol.com",
  "first_name": "Jose",
  "last_name": "Gaspar",
  "is_admin": false,
  "is_seller": true
 },
 {
  "uuid": "3c1b0efc-ddd7-4152-940f-235cdf36ec5d",
  "email": "email@email.com",
  "first_name": "Alex",
  "last_name": "Silva",
  "is_admin": true,
  "is_seller": false
 },
 {
  "uuid": "5bfaaea7-656a-4435-a87b-03526c6dc821",
  "email": "customer@gmail.com",
  "first_name": "customer",
  "last_name": "one",
  "is_admin": false,
  "is_seller": false
 }
]
```

Caso o usuário não seja administrador, o retorno será:

RESPONSE STATUS -> HTTP 403(Forbidden)

```json
{
 "detail": "You do not have permission to perform this action."
}
```

### Fee

**POST /api/fee**

Rota para adicionar uma taxa que será deduzido aos recebíveis de acordo com o método de pagamento

Authorization Header:

**Necessário passar o token para autenticação e autorização**

**Somente o Administrador tem acesso a rota**

RESPONSE STATUS -> HTTP 201(Created)

Body:

```json
{
 "credit_fee": 0.05,
 "debit_fe": 0.03
}
```

Response:

```json
{
 "uuid": "5a3acfb4-6441-4a7b-bc9e-a1184f1303d4",
 "credit_fee": 0.05,
 "debit_fee": 0.03,
 "created_at": "2022-03-18T13:40:44.527467Z"
}
```

**GET /api/fee**

Rota para listar todas as taxas cadastradas no sistema

Authorization Header:

**Necessário passar o token para autenticação e autorização**

**Somente o Administrador tem acesso a rota**

RESPONSE STATUS -> HTTP 200(Ok)

Body:

_No body_

Response:

```json
[
 {
  "uuid": "c54ebee9-c2fe-4fa8-92d2-963055455d41",
  "credit_fee": 0.06,
  "debit_fee": 0.04,
  "created_at": "2022-03-17T13:35:54.676822Z"
 },
 {
  "uuid": "e5f9a1ea-b405-4b8c-a8a6-0e85632402f7",
  "credit_fee": 0.05,
  "debit_fee": 0.03,
  "created_at": "2022-03-17T13:36:36.808176Z"
 }
]
```

**GET /api/fee/<fee_id>**

Rota para listar uma taxa cadastrada no sistema

Authorization Header:

**Necessário passar o token para autenticação e autorização**

**Somente o Administrador tem acesso a rota**

RESPONSE STATUS -> HTTP 200(Ok)

Body:

_No body_

Response:

```json
[
 {
  "uuid": "e5f9a1ea-b405-4b8c-a8a6-0e85632402f7",
  "credit_fee": 0.05,
  "debit_fee": 0.03,
  "created_at": "2022-03-17T13:36:36.808176Z"
 }
]
```

### Payment Info

**POST /api/payment_info**

Rota para criar uma informação de pagamento

Authorization Header:

**Necessário passar o token para autenticação**

**Somente o Cliente tem acesso a rota**

RESPONSE STATUS -> HTTP 201(Created)

Body:

````json
{
  "payment_method": "credit",
  "card_number": "1234885468854",
  "cardholders_name": "Alexander P Silva",
  "card_expiring_date": "2022-06-01",
  "cvv": "456"
}

Response:

```json
{
  "uuid": "3feeea8f-8cd7-4d70-aaac-bd2fc9947c28",
  "customer": {
    "uuid": "5bfaaea7-656a-4435-a87b-03526c6dc821",
    "email": "customer@gmail.com",
    "first_name": "customer",
    "last_name": "one",
    "is_admin": false,
    "is_seller": false
  },
  "card_number_info": "8854",
  "payment_method": "credit",
  "cardholders_name": "Alexander P Silva",
  "card_expiring_date": "2022-06-01",
  "is_active": true
}
````

Caso o usuário não seja _customer_ o retorno será:

RESPONSE STATUS -> HTTP 403(Forbidden)

```json
{
 "detail": "You do not have permission to perform this action."
}
```

Caso o cartão informado tenha vencido, o retorno será:

RESPONSE STATUS -> HTTP 400(Bad Request)

```json
{
 "error": ["This card is expired"]
}
```

**GET /api/fee**

Rota para listar todos os métodos de pagamento do próprio consumidor

Authorization Header:

**Necessário passar o token para autenticação**

RESPONSE STATUS -> HTTP 200(Ok)

Body:

_No body_

Response:

```json
[
 {
  "uuid": "422a1708-69de-49e8-86e6-afed244031f0",
  "customer": "5bfaaea7-656a-4435-a87b-03526c6dc821",
  "card_number_info": "5678",
  "payment_method": "debit",
  "cardholders_name": "MARIANA F SOUZA",
  "card_expiring_date": "2022-04-01",
  "is_active": true
 },
 {
  "uuid": "59e9797d-6e27-4211-ae92-4c8daf779cd4",
  "customer": "5bfaaea7-656a-4435-a87b-03526c6dc821",
  "card_number_info": "6789",
  "payment_method": "credit",
  "cardholders_name": "MARIANA F SOUZA",
  "card_expiring_date": "2022-04-01",
  "is_active": true
 },
 {
  "uuid": "3feeea8f-8cd7-4d70-aaac-bd2fc9947c28",
  "customer": "5bfaaea7-656a-4435-a87b-03526c6dc821",
  "card_number_info": "8854",
  "payment_method": "credit",
  "cardholders_name": "Alexander P Silva",
  "card_expiring_date": "2022-06-01",
  "is_active": true
 }
]
```

### Products

**POST /api/products**

Rota para criar um produto pelo vendedor:

Authorization Header:

**Necessário passar o token para autenticação**

**Somente o Vendedor tem acesso a rota**

RESPONSE STATUS -> HTTP 201(Created)

Body:

```json
{
 "description": "Celular Samsung",
 "price": 800.99,
 "quantity": 15
}
```

Response:

```json
{
 "uuid": "6761e799-c125-434b-ba72-66c94c27298d",
 "seller": {
  "uuid": "437e93b6-84b7-4f93-9a59-a2ae05ab0e09",
  "email": "jose@bol.com",
  "first_name": "Jose",
  "last_name": "Gaspar",
  "is_admin": false,
  "is_seller": true
 },
 "description": "Celular Samsung",
 "price": 800.99,
 "quantity": 15,
 "is_active": true
}
```

**PATCH /api/products/<product_id>**

Rota para atualização de um produto

Authorization Header:

**Necessário passar o token para autenticação**

**Somente o Vendedor tem acesso a rota**

RESPONSE STATUS -> HTTP 200(Ok)

Body:

```json
{
 "description": "Celular Iphone 10"
}
```

Response:

```json
{
 "uuid": "6761e799-c125-434b-ba72-66c94c27298d",
 "seller": {
  "uuid": "437e93b6-84b7-4f93-9a59-a2ae05ab0e09",
  "email": "jose@bol.com",
  "first_name": "Jose",
  "last_name": "Gaspar",
  "is_admin": false,
  "is_seller": true
 },
 "description": "Celular Iphone 10",
 "price": 800.99,
 "quantity": 15,
 "is_active": true
}
```

**GET /api/products**

Rota para listar todos os produtos no sietema

Authorization Header:

**Não é necessário nenhum tipo de autenticação**

RESPONSE STATUS -> HTTP 200(Ok)

Body:

_No body_

Response:

```json
[
 {
  "uuid": "ed696a09-7304-4df1-bf4c-44216de953e8",
  "seller": "437e93b6-84b7-4f93-9a59-a2ae05ab0e09",
  "description": "Smartband XYZ 3.0",
  "price": 100.99,
  "quantity": 15,
  "is_active": true
 },
 {
  "uuid": "6761e799-c125-434b-ba72-66c94c27298d",
  "seller": "437e93b6-84b7-4f93-9a59-a2ae05ab0e09",
  "description": "Celular Iphone 10",
  "price": 800.99,
  "quantity": 15,
  "is_active": true
 }
]
```

**GET /api/products/<product_id>**

Rota para listar um produto pelo id

Authorization Header:

**Não é necessário nenhum tipo de autenticação**

RESPONSE STATUS -> HTTP 200(Ok)

Body:

_No body_

Response:

```json
{
  "uuid": "ed696a09-7304-4df1-bf4c-44216de953e8",
  "seller": "437e93b6-84b7-4f93-9a59-a2ae05ab0e09",
  "description": "Smartband XYZ 3.0",
  "price": 100.99,
  "quantity": 15,
  "is_active": true
}
```

**GET /api/products/seller/<seller_id>**

Rota para listar um ou mais produtos de um determinado vendedor

Authorization Header:

**Não é necessário nenhum tipo de autenticação**

RESPONSE STATUS -> HTTP 201(No Content)

Body:

_No body_

Response:

```json
[
  {
    "uuid": "ed696a09-7304-4df1-bf4c-44216de953e8",
    "seller": "437e93b6-84b7-4f93-9a59-a2ae05ab0e09",
    "description": "Smartband XYZ 3.0",
    "price": 100.99,
    "quantity": 15,
    "is_active": true
  },
  {
    "uuid": "1c11d480-80d2-4081-b3c5-b18a2993e06b",
    "seller": "437e93b6-84b7-4f93-9a59-a2ae05ab0e09",
    "description": "Celular Samsung",
    "price": 800.99,
    "quantity": 15,
    "is_active": true
  },
  {
    "uuid": "6761e799-c125-434b-ba72-66c94c27298d",
    "seller": "437e93b6-84b7-4f93-9a59-a2ae05ab0e09",
    "description": "Celular Iphone 10",
    "price": 800.99,
    "quantity": 15,
    "is_active": true
  }
]
```

## Tecnologias e linguagens utilizadas

- Python
- Django
- djangorestframework
- Generic Views
